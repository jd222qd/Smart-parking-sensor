# Smart-parking-sensor


**Project Overview**

Finding free parking spots can often be a time consuming process and cause overcrowding
within bigger cities. The purpose of this project is to help reduce overcrowding in cities where
there are many cars. If information is available about the number of free parking spots before
a car enters the parking lot, this would in theory reduce overcrowding in that area. It also
makes it easier for people to find free parking spots. Another motivation for the project is the
ability to see if private parking spots are being used by other, unauthorized cars.This Is
useful for companies and organizations that require their parking spots to be available at all
times. By being able to detect unauthorized parking straight away, organizations may quickly
get in contact with a towing company to remove the unauthorized vehicle from the parking
spot, before it causes any major issues.

To solve these problems, a parking sensor is to be created, programmed and mounted on
said parking spots in order to monitor the activity of cars and other vehicles. This is done by
using a magnetometer sensor that can detect the magnetic fields that vehicles produce and
therefore detect that they are present in the parking spot. The magnetometer sensor is in
turn connected to a Pycom Lopy4 device with a LoRa antenna, mounted on an Expansion
Board 3.1. This setup is powered by a 3.7V LiPo battery and surrounded by an outer shell, to
protect the components. The device communicates with a TTN network using LoRa, which in
turn communicates with a web page. Whenever a car is detected by the magnetometer
sensor, a signal will be sent to the TTN network, using LoRa, and then displayed on ubidots
by integration with the TTN network.

**Used Components**

The following hardware components listed below were used in the project:
- Pycom Lopy4
- Pycom Expansions Board v3.1
- Pycom LoRa Antenna
- 3-Axis Magnetic Sensor QMC5883L
- 3.7V LiPo Battery
- Jumper Wires (male/male)
- Breadboard
- Outer shell/casing

The following software components listed below were used in the project:
- LoRaWAN, wireless technology
- TTN, The Things Network
- Ubidots

